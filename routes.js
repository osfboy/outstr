
exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	mdbClient.connect("mongodb://localhost:27017/test", function(err, db) {
		var collection = db.collection('products');

		collection.find().toArray(function(err, items) {
			res.render("index", {
				title : "Outlet Store",
                nav: [{
                    Link: '/mens',
                    Text: 'Mens'
                }, {
                    Link: '/women',
                    Text: 'Womens'
                }],
				items : items
			});

			db.close();
		});


	});
};



exports.mens = function (req,res) {
    var _         = require("underscore");
    var mdbClient = require('mongodb').MongoClient;

    mdbClient.connect("mongodb://localhost:27017/test", function(err, db) {
        var collection = db.collection('categories');

        collection.find().toArray(function(err, items) {
            res.render("mens", {
                title : "Outlet Store",
                nav: [{
                    Link: '/mens',
                    Text: 'Mens'
                }, {
                    Link: '/women',
                    Text: 'Womens'
                }],
                itemsM : items
            });

            db.close();
        });
    });
};


exports.women = function(req, res) {
    var _         = require("underscore");
    var mdbClient = require('mongodb').MongoClient;

    mdbClient.connect("mongodb://localhost:27017/test", function(err, db) {
        var collection = db.collection('categories');

        collection.find().toArray(function(err, items) {
            res.render("women", {
                title : "Outlet Store",
                nav: [{
                    Link: '/mens',
                    Text: 'Mens'
                }, {
                    Link: '/women',
                    Text: 'Womens'
                }],
                itemsW : items
            });

            db.close();
        });


    });
};

exports.itemView = function(req, res) {
    var _         = require("underscore");
    var mdbClient = require('mongodb').MongoClient;
    var objectId = require('mongodb').ObjectID;
    var id = new objectId(req.params.id);
    mdbClient.connect("mongodb://localhost:27017/test", function(err, db) {
        var collection = db.collection('products');

        collection.findOne({_id: id}, function(err, items) {
            res.render("itemView", {
                title : "Outlet Store",
                nav: [{
                    Link: '/mens',
                    Text: 'Mens'
                }, {
                    Link: '/women',
                    Text: 'Womens'
                }],
                item : items
            });

            db.close();
        });
    });

};