var express = require('express');
var itemRouter = express.Router();
var mongodb = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;

var router = function (nav) {

    itemRouter.route("/index")
        .get(function (req,res) {
            var url = 'mongodb://localhost:27017/test';

            mongodb.connect(url, function (err, db) {
                var collection = db.collection('products');

                collection.find({}).toArray(
                    function (err, results) {
                        res.render('index', {
                            title: 'Outlet Store',
                            nav: nav,
                            items: results
                        });
                    }
                );
            });
        });


    itemRouter.route("/:id")
        .get(function (req,res) {
            var id = new objectId(req.params.id);
            var url = 'mongodb://localhost:27017/test';

            mongodb.connect(url, function (err, db) {
                var collection = db.collection('products');

                collection.findOne({_id : id},
                    function (err, results) {
                        res.render('itemView', {
                            title: 'Outlet Store',
                            nav: nav,
                            item: results
                        });
                    }
                );
            });
        });
    return itemRouter;

};
module.exports = router;